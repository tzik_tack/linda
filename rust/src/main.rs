extern crate fs2;
extern crate getopts;
extern crate inotify;
extern crate libc;
#[macro_use]
extern crate log;
extern crate regex;
extern crate simple_logger;

use fs2::FileExt;
use getopts::Options;
use inotify::Inotify;
use inotify::WatchMask;
use libc::pid_t;
use regex::Regex;
use std::env;
use std::fs::File;
use std::fs::OpenOptions;
use std::fs;
use std::io::BufRead;
use std::io::BufReader;
use std::io::Read;
use std::io::Seek;
use std::io::SeekFrom;
use std::io::Write;
use std::os::unix::net::UnixDatagram;
use std::path::Path;
use std::path::PathBuf;
use std::process;

fn pid() -> pid_t {
    unsafe { libc::getpid() }
}

trait MutUnWrap<T> {
    fn munw(&mut self) -> &mut T;
}

impl<T> MutUnWrap<T> for Option<T> {
    fn munw(&mut self) -> &mut T {
        return self.as_mut().unwrap();
    }
}


struct Linda {
    working_directory: PathBuf,
    lock_file: PathBuf,
    data_file: PathBuf,
    tmp_file: PathBuf,
    wait_socket: PathBuf,
    notify_socket: PathBuf,
    blocking: bool,
    command: String,
    data: String,
}

impl Linda {
    fn new() -> Linda {
        Linda {
            working_directory: PathBuf::new(),
            lock_file:  PathBuf::new(),
            data_file: PathBuf::new(),
            tmp_file: PathBuf::new(),
            wait_socket: PathBuf::new(),
            notify_socket: PathBuf::new(),
            blocking: true,
            command: String::new(),
            data: String::new(),
        }
    }
    fn set_files(&mut self) {
        self.working_directory =
            match env::var("LINDA_HOME") {
                Ok(val) => Path::new(&val).to_path_buf(),
                Err(_) => Path::new(&env::var("HOME").unwrap())
                    .join(Path::new(".linda"))
            };
        info!("working_directory = {:?}", self.working_directory);
        self.lock_file = self.working_directory.join("linda.lock");
        info!("lock_file = {:?}", self.lock_file);
        self.data_file = self.working_directory.join("linda.dat");
        info!("data_file = {:?}", self.data_file);
        self.tmp_file = self.working_directory.join("linda.tmp");
        info!("tmp_file = {:?}", self.tmp_file);
        self.wait_socket = self.working_directory.join(
            format!("wait.{}", pid()));
        info!("wait_socket = {:?}", self.wait_socket);
        self.notify_socket = self.working_directory.join("notify");
        info!("notify_socket = {:?}", self.notify_socket);
    }
    fn parse_args(&mut self) {
        let args: Vec<String> = env::args().collect();
        let mut opts = Options::new();
        opts.optflag("v", "verbose", "show verbose log");
        opts.optflag("p", "unblocking", "unblocking");
        let r = opts.parse(&args[1..]).unwrap();
        if r.opt_present("v") {
            simple_logger::init().unwrap();
            info!("enable logging");
        }
        if r.opt_present("p") {
            self.blocking = false;
            info!("enable unblocking");
        }
        info!("args = {:?}", r.free);
        if r.free.len() < 1 {
            panic!("Needs commands");
        }
        if r.free.len() < 2 {
            panic!("Needs data");
        }
        self.command = r.free[0].clone();
        info!("command = {}", self.command);
        self.data = r.free[1].clone();
        info!("data = {}", self.data);
    }
    fn lock(&self) -> File {
        info!("lock");
        let f = OpenOptions::new()
            .create(true)
            .append(true)
            .open(&self.lock_file)
            .unwrap();
        f.lock_exclusive().unwrap();
        return f;
    }
    fn prepare_repository(&self) {
        info!("prepare_repository");
        fs::create_dir_all(&self.working_directory).unwrap();
        let _l = self.lock();
        OpenOptions::new()
            .create(true)
            .write(true)
            .open(&self.data_file)
            .unwrap();

    }
    fn run_command(mut self) -> i32 {
        info!("run_command");
        info!("dispatch!");
        match self.command.as_str() {
            "out" => {
                return self.out_command();
            }
            "rd" => {
                return self.rd_command();
            }
            "rdp" => {
                self.blocking = false;
                return self.rd_command();
            }
            "in" => {
                return self.in_command();
            }
            "inp" => {
                self.blocking = false;
                return self.in_command();
            }
            _ => {
                panic!("Unknown command {}.", self.command);
            }
        }
    }
    fn out_command(self) -> i32 {
        info!("out_command");
        info!("lock!");
        let _l = self.lock();
        let mut f = OpenOptions::new()
            .create(true)
            .append(true)
            .open(&self.data_file)
            .unwrap();
        writeln!(f, "{}", self.data).unwrap();
        if !cfg!(feature = "use_inotify") {
            let _ = fs::remove_file(&self.notify_socket);
            let socket = UnixDatagram::bind(&self.notify_socket).unwrap();
            for f in fs::read_dir(&self.working_directory).unwrap() {
                let p = f.unwrap().path();
                let name = p.file_stem().unwrap();
                info!("Check file name: {:?}", p);
                info!("wait socket?: {}", name == "wait");
                if name == "wait" {
                    let r = socket.send_to(b"notify", &p);
                    if !r.is_ok() {
                        info!("Failed to notify delete the file.");
                        fs::remove_file(&p).unwrap();
                    }
                }
            }
        }
        return 0;
    }
    fn maybe_wait_until_success<F>(&self, f: F) -> i32 where F : Fn() -> i32 {
        info!("maybe_wait_until_success");
        let mut n :Option<Inotify> = None;
        let mut s :Option<UnixDatagram> = None;
        if cfg!(feature = "use_inotify") {
            n = Some(Inotify::init().unwrap());
            n.munw().add_watch(
                &self.working_directory, WatchMask::MODIFY).unwrap();
        } else {
            s = Some(UnixDatagram::bind(&self.wait_socket).unwrap());
        }
        loop {
            let ret = f();
            if ret == 0 || !self.blocking {
                info!("escape!");
                if !cfg!(feature = "use_inotify") {
                    fs::remove_file(&self.wait_socket).unwrap();
                }
                return ret;
            }
            info!("sleep");
            if cfg!(feature = "use_inotify") {
              let mut buffer = [0;1024];
              let _ = n.munw().read_events_blocking(
                  &mut buffer).unwrap();
            } else {
              let mut buffer = [0;1024];
              let (count,address) = s.munw().recv_from(&mut buffer).unwrap();
              info!("socket {:?} sent {:?}", address, &buffer[..count]);
            }
            info!("wake up");
        }
    }
    fn rd_command(self) -> i32 {
        info!("rd_command");
        let m = Regex::new(&self.data).unwrap();
        return self.maybe_wait_until_success(||{
            info!("lock!");
            let _l = self.lock();
            let r = BufReader::new(File::open(&self.data_file).unwrap());
            for line in r.lines() {
                let l = line.unwrap();
                if m.is_match(l.as_str()) {
                    println!("{}", l);
                    return 0
                }
            }
            return 1;
        })
    }
    fn in_command(self) -> i32 {
        info!("in_command");
        let m = Regex::new(&self.data).unwrap();
        return self.maybe_wait_until_success(||{
            info!("lock!");
            let _l = self.lock();
            let mut r = BufReader::new(File::open(&self.data_file).unwrap());
            let mut found = false;
            for line in r.by_ref().lines() {
                let l = line.unwrap();
                if m.is_match(l.as_str()) {
                    println!("{}", l);
                    found = true;
                    break;
                }
            }
            if !found { return 1 };
            r.seek(SeekFrom::Start(0)).unwrap();
            {
                let mut w = OpenOptions::new()
                    .create(true)
                    .write(true)
                    .open(&self.tmp_file)
                    .unwrap();
                let mut found = false;
                for line in r.lines() {
                    let l = line.unwrap();
                    if !found && m.is_match(l.as_str()) {
                        found = true;
                    } else {
                        writeln!(w, "{}", l).unwrap();
                    }
                }
            }
            fs::rename(&self.tmp_file, &self.data_file).unwrap();
            return 0;
        })
    }
}

fn main(){
    let mut linda = Linda::new();
    linda.parse_args();
    linda.set_files();
    linda.prepare_repository();
    process::exit(linda.run_command());
}
