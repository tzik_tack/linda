#pragma once

#include <atomic>
#include <memory>
#include <pthread.h>

struct LindaLock {
  std::atomic<bool> ready;
  pthread_mutex_t mutex;
  pthread_cond_t cond;
  uint64_t revision;

  struct Deleter {
    void operator()(const LindaLock* lock) const;
  };

  using Ptr = std::unique_ptr<LindaLock, Deleter>;
  static Ptr Create(const char* lock_file);

  void Lock();
  void Unlock();
  void WaitForNewRevision();
  void IncrementAndNotify();
};

struct ScopedLock {
  ScopedLock(LindaLock* lock) : lock(lock) {
    lock->Lock();
  }

  void Wait() {
    lock->WaitForNewRevision();
  }

  void Notify() {
    lock->IncrementAndNotify();
  }

  ~ScopedLock() {
    lock->Unlock();
  }

  LindaLock* lock;
};
