
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <getopt.h>
#include <iostream>
#include <limits.h>
#include <regex>
#include <string>
#include <sys/file.h>
#include <sys/inotify.h>
#include <sys/stat.h>
#include <unistd.h>

#include "linda_lock.h"

using namespace std;

LindaLock::Ptr lindaLock;
string* workingDirectory;
string* dataFile;
string* tmpFile;

bool nonblocking = false;

void error(const string& msg) {
  cerr << msg << endl;
  exit(2);
}

void initializeFileLocations() {
  clog << "initializeFileLocations" << endl;
  if (const char* LINDA_HOME = getenv("LINDA_HOME")) {
    clog << "LINDA_HOME: " << LINDA_HOME << endl;
    workingDirectory = new string(LINDA_HOME);
  } else if (const char* HOME = getenv("HOME")) {
    clog << "HOME: " << HOME << endl;
    workingDirectory = new string(string(HOME)+"/.linda");
  } else {
    error("Couldn't find HOME");
  }
  clog << "workingDirectory: " << *workingDirectory << endl;
  dataFile = new string(*workingDirectory + "/linda.dat");
  clog << "dataFile: " << *dataFile << endl;
  tmpFile = new string(*workingDirectory + "/linda.tmp");
  clog << "tmpFile: " << *tmpFile << endl;
}

void parseArguments(int argc, const char** argv, string* command, string* data) {
  while (true) {
    int opt = getopt(argc, const_cast<char**>(argv), "vp");
    clog << "opt: " << opt << endl;
    if (opt == -1) break;
    clog << "opt: " << static_cast<char>(opt) << endl;
    switch (opt) {
      case 'v':
        clog.clear();
        break;
      case 'p':
        nonblocking = true;
        break;
      default:
        clog << "argc: " << argc << endl;
        for (int i=0;i<argc;i++) {
          clog << "argv(" << i << "): " << argv[i] << endl;
        }
        clog << "Unknown param at " << optind-1 << " which is " << argv[optind-1] << endl;
        error("Usage linda [-v] [-p] (out|in|inp|rd|rdp) data");
        break;
    }
  }
  clog << "optind: " << optind << endl;
  if (argc < optind + 1) {
    error("Needs commands");
  }
  if (argc < optind + 2) {
    error("Needs data");
  }
  *command = argv[optind];
  clog << "command: " << *command << endl;
  *data = argv[optind+1];
  clog << "data: " << *data << endl;
  clog << "nonblocking: " << (nonblocking?"true":"false") << endl;
}

void prepareRepository() {
   mkdir(workingDirectory->c_str(), 0777);
   ofstream dat(*dataFile, ofstream::app);
}

int outCommand(const string& text) {
  clog << "outCommand" << endl;
  ScopedLock l(lindaLock.get());
  prepareRepository();
  ofstream dat(*dataFile, ofstream::app);
  dat << text << endl;
  dat.close();
  clog << "write: " << text << endl;
  l.Notify();
  return 0;
}

int rdCommand(string pattern) {
  clog << "rdCommand" << endl;
  regex re(pattern);
  ScopedLock l(lindaLock.get());
  prepareRepository();
  while (true) {
    ifstream dat(*dataFile);
    string line;
    while(getline(dat, line)) {
      clog << "line: " << line << endl;
      if (regex_match(line, re)) {
        clog << "found" << endl;
        cout << line << endl;
        return 0;
      }
    }
    clog << "not found" << endl;
    l.Wait();
  }
}

int inCommand(string pattern) {
  clog << "inCommand" << endl;
  regex re(pattern);
  ScopedLock l(lindaLock.get());
  prepareRepository();
  while (true) {
    ifstream dat(*dataFile);
    bool found = false;
    string line;
    while(getline(dat, line)) {
      clog << "line: " << line << endl;
      if (!found && regex_match(line, re)) {
        clog << "match" << endl;
        cout << line << endl;
        found = true;
        break;
      } else {
        clog << "not match" << endl;
      }
    }
    if (!found) {
      l.Wait();
      continue;
    }
    dat.seekg(0, dat.beg);
    ofstream tmp(*tmpFile);
    found = false;
    while(getline(dat, line)) {
      clog << "line: " << line << endl;
      if (!found && regex_match(line, re)) {
        clog << "match" << endl;
        found = true;
      } else {
        clog << "not match" << endl;
        tmp << line << endl;
      }
    }
    tmp.close();
    rename(tmpFile->c_str(), dataFile->c_str());
    clog << "commit" << endl;
    return 0;
  }
}

int main(int argc, const char** argv) {
  clog.setstate(ios_base::failbit);
  string command;
  string data;
  parseArguments(argc, argv, &command, &data);
  initializeFileLocations();

  // TODO: Use unique name for the working directory.
  lindaLock = LindaLock::Create("linda.lock");

  clog << "linda" << endl;
  int rv = 2;
  if (command == "out") {
    rv = outCommand(data);
  } else if (command == "rd") {
    rv = rdCommand(data);
  } else if (command == "rdp") {
    nonblocking = true;
    rv = rdCommand(data);
  } else if (command == "in") {
    rv = inCommand(data);
  } else if (command == "inp") {
    nonblocking = true;
    rv = inCommand(data);
  } else {
    error("Unknown command");
  }
  delete workingDirectory;
  delete dataFile;
  delete tmpFile;
  return rv;
}
