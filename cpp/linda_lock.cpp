
#include "linda_lock.h"

#include <fcntl.h>
#include <pthread.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

void LindaLock::Lock() {
  if (pthread_mutex_lock(&mutex))
    abort();
}

void LindaLock::Unlock() {
  if (pthread_mutex_unlock(&mutex))
    abort();
}

void LindaLock::WaitForNewRevision() {
  uint64_t old = revision;
  while (old == revision)
    pthread_cond_wait(&cond, &mutex);
}

void LindaLock::IncrementAndNotify() {
  ++revision;
  if (pthread_cond_broadcast(&cond))
    abort();
}

void LindaLock::Deleter::operator()(const LindaLock* lock) const {
  munmap(const_cast<LindaLock*>(lock), sizeof(LindaLock));
}

LindaLock::Ptr LindaLock::Create(const char* lock_file) {
  int fd = shm_open(lock_file, O_RDWR | O_CREAT | O_EXCL, 0600);
  bool needs_initialization = false;
  if (fd >= 0) {
    if (ftruncate(fd, sizeof(LindaLock)) < 0) {
      perror("ftruncate: ");
      abort();
    }
    needs_initialization = true;
  } else {
    fd = shm_open(lock_file, O_RDWR, 0600);
    if (fd < 0) {
      perror("shm_open: ");
      abort();
    }
  }

  void* buf = mmap(nullptr, sizeof(LindaLock), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
  if (buf == MAP_FAILED) {
    perror("mmap: ");
    abort();
  }

  LindaLock* lock = static_cast<LindaLock*>(buf);
  close(fd);

  if (needs_initialization) {
    pthread_mutex_init(&lock->mutex, nullptr);

    pthread_condattr_t condattr;
    pthread_condattr_init(&condattr);
    if (pthread_condattr_setpshared(&condattr, PTHREAD_PROCESS_SHARED) < 0)
      abort();
    pthread_cond_init(&lock->cond, &condattr);
    pthread_condattr_destroy(&condattr);

    lock->revision = 0;
    lock->ready = true;
  } else {
    while (!lock->ready)
      sched_yield();
  }
  return LindaLock::Ptr(lock);
}
