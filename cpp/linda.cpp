#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <getopt.h>
#include <iostream>
#include <limits.h>
#include <regex>
#include <string>
#include <sys/file.h>
#include <sys/inotify.h>
#include <sys/stat.h>
#include <unistd.h>

using namespace std;

string* workingDirectory;
string* lockFile;
string* dataFile;
string* tmpFile;

bool nonblocking = false;

void error(const string& msg) {
  cerr << msg << endl;
  exit(2);
}

void initializeFileLocations() {
  clog << "initializeFileLocations" << endl;
  if (const char* LINDA_HOME = getenv("LINDA_HOME")) {
    clog << "LINDA_HOME: " << LINDA_HOME << endl;
    workingDirectory = new string(LINDA_HOME);
  } else if (const char* HOME = getenv("HOME")) {
    clog << "HOME: " << HOME << endl;
    workingDirectory = new string(string(HOME)+"/.linda");
  } else {
    error("Couldn't find HOME");
  }
  clog << "workingDirectory: " << *workingDirectory << endl;
  lockFile = new string(*workingDirectory + "/linda.lock");
  clog << "lockFile: " << *lockFile << endl;
  dataFile = new string(*workingDirectory + "/linda.dat");
  clog << "dataFile: " << *dataFile << endl;
  tmpFile = new string(*workingDirectory + "/linda.tmp");
  clog << "tmpFile: " << *tmpFile << endl;
}

void parseArguments(int argc, const char** argv, string* command, string* data) {
  while (true) {
    int opt = getopt(argc, const_cast<char**>(argv), "vp");
    clog << "opt: " << opt << endl;
    if (opt == -1) break;
    clog << "opt: " << static_cast<char>(opt) << endl;
    switch (opt) {
      case 'v':
        clog.clear();
        break;
      case 'p':
        nonblocking = true;
        break;
      default:
        clog << "argc: " << argc << endl;
        for (int i=0;i<argc;i++) {
          clog << "argv(" << i << "): " << argv[i] << endl;
        }
        clog << "Unknown param at " << optind-1 << " which is " << argv[optind-1] << endl;
        error("Usage linda [-v] [-p] (out|in|inp|rd|rdp) data");
        break;
    }
  }
  clog << "optind: " << optind << endl;
  if (argc < optind + 1) {
    error("Needs commands");
  }
  if (argc < optind + 2) {
    error("Needs data");
  }
  *command = argv[optind];
  clog << "command: " << *command << endl;
  *data = argv[optind+1];
  clog << "data: " << *data << endl;
  clog << "nonblocking: " << (nonblocking?"true":"false") << endl;
}

class Lock {
 public:
  Lock() {
    fd = open(lockFile->c_str(), O_WRONLY | O_CREAT, 0777);
    if (fd == -1) error("Failed to open lock");
    int r = flock(fd, LOCK_EX);
    if (r != 0) error("Failed to get lock");
    clog << "lock accuired" << endl;
  }
  ~Lock() {
    close(fd);
    clog << "lock released" << endl;
  }
 private:
  int fd;
};

void prepareRepository() {
   mkdir(workingDirectory->c_str(), 0777);
   Lock l;
   ofstream dat(*dataFile, ofstream::app);
}

int outCommand(const string& text) {
  clog << "outCommand" << endl;
  prepareRepository();
  Lock l;
  ofstream dat(*dataFile, ofstream::app);
  dat << text << endl;
  dat.close();
  clog << "write: " << text << endl;
  return 0;
}

template <typename F>
int maybeWaitUntilSuccess(F fn) {
#ifndef USE_SLEEP
  int fd = inotify_init();
  if (fd == -1) error("failed inotify_init");
  if (-1 == inotify_add_watch(fd, workingDirectory->c_str(), IN_MODIFY)) {
    error("failed inotify_add_watch");
  }
#endif
  int ret;
  while (true) {
    ret = fn();
    if (ret == 0 || nonblocking) break;
    clog << "sleep" << endl;
#ifndef USE_SLEEP
    clog << "use inotify" << endl;
    char buf[sizeof(inotify_event) + NAME_MAX + 1];
    auto bytes = read(fd, buf, sizeof(buf));
    if (bytes == 0) error("read failed");
#else
    clog << "use sleep" << endl;
    usleep(10000);
#endif
    clog << "wake up" << endl;
  }
#ifndef USE_SLEEP
  close(fd);
#endif
  return ret;
}

int rdCommand(string pattern) {
  clog << "rdCommand" << endl;
  regex re(pattern);
  prepareRepository();
  return maybeWaitUntilSuccess([re](){
    Lock l;
    ifstream dat(*dataFile);
    string line;
    while(getline(dat, line)) {
      clog << "line: " << line << endl;
      if (regex_match(line, re)) {
        clog << "found" << endl;
        cout << line << endl;
        return 0;
      }
    }
    clog << "not found" << endl;
    return 1;
  });
}

int inCommand(string pattern) {
  clog << "inCommand" << endl;
  regex re(pattern);
  prepareRepository();
  return maybeWaitUntilSuccess([re](){
    Lock l;
    ifstream dat(*dataFile);
    bool found = false;
    string line;
    while(getline(dat, line)) {
      clog << "line: " << line << endl;
      if (!found && regex_match(line, re)) {
        clog << "match" << endl;
        cout << line << endl;
        found = true;
        break;
      } else {
        clog << "not match" << endl;
      }
    }
    if (!found) return 1;
    dat.seekg(0, dat.beg);
    ofstream tmp(*tmpFile);
    found = false;
    while(getline(dat, line)) {
      clog << "line: " << line << endl;
      if (!found && regex_match(line, re)) {
        clog << "match" << endl;
        found = true;
      } else {
        clog << "not match" << endl;
        tmp << line << endl;
      }
    }
    tmp.close();
    rename(tmpFile->c_str(), dataFile->c_str());
    clog << "commit" << endl;
    return 0;
  });
}

int main(int argc, const char** argv) {
  clog.setstate(ios_base::failbit);
  string command;
  string data;
  parseArguments(argc, argv, &command, &data);
  initializeFileLocations();
  clog << "linda" << endl;
  int rv = 2;
  if (command == "out") {
    rv = outCommand(data);
  } else if (command == "rd") {
    rv = rdCommand(data);
  } else if (command == "rdp") {
    nonblocking = true;
    rv = rdCommand(data);
  } else if (command == "in") {
    rv = inCommand(data);
  } else if (command == "inp") {
    nonblocking = true;
    rv = inCommand(data);
  } else {
    error("Unknown command");
  }
  delete workingDirectory;
  delete lockFile;
  delete dataFile;
  delete tmpFile;
  return rv;
}
